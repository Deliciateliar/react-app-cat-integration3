import React, { useState, useEffect } from "react";
import "./App.css";
import ReactPaginate from "react-paginate";

const App = () => {
  const [catUrl, setCatUrl] = useState([]);

  useEffect(() => {
    const getCat = async () => {
      const res = await fetch(
        "https://api.thecatapi.com/v1/images/search?limit=9&page=100&order=DESC"
      );
      const data = await res.json();
      setCatUrl(data);
    };
    getCat();
  }, []);
  console.log(catUrl);

  const fetchCatUrl = async (currentPage)=>{
    const res = await fetch(
      // eslint-disable-next-line no-template-curly-in-string
      "https://api.thecatapi.com/v1/images/search?limit=9&page=${currentPage}&order=DESC"
    );
    const data = await res.json();
    return data;
  }
  const handlePageClick = async (data) => {
    console.log(data.selected);

    let currentPage = data.selected + 1

    const catImagesServer =  await fetchCatUrl(currentPage)
    setCatUrl(catImagesServer);
  };
  
  return (
    <div className="container">
      <div className="row m-2">
        {catUrl.map((catUrls) => {
          return (
            <div className="col-sm-6 col-md-4 v my-2">
              <div className="card shadow-sm w-100">
                <div className="card-body">
                  <img src={catUrls.url} alt="cat" href={catUrls.url}/>
                </div>
              </div>
            </div>
          );
        })}
      </div>

      <ReactPaginate
        previousLabel={"Page précédente"}
        nextLabel={"Page suivante"}
        breakLabel={"..."}
        pageCount={20}
        marginPagesDisplayed={5}
        pageRangeDisplayed={3}
        onPageChange={handlePageClick}
        containerClassName={"pagination justify-content-center"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        nextClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />
    </div>
  );
};

export default App;


